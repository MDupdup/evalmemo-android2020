package com.formationandroid.evalmemo.recyclerview

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.evalmemo.R

class MemoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var textViewMemoLabel: TextView = itemView.findViewById(R.id.recycler_view_item_text)
}
