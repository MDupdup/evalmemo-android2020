package com.formationandroid.evalmemo.recyclerview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.evalmemo.DetailFragment
import com.formationandroid.evalmemo.R
import com.formationandroid.evalmemo.activities.DetailActivity
import com.formationandroid.evalmemo.activities.MainActivity
import com.formationandroid.evalmemo.models.Memo
import com.formationandroid.evalmemo.repositories.MainRepository
import java.util.*

class MemoAdapter(private var memoList: MutableList<Memo>, private val ctx: Context) :
    RecyclerView.Adapter<MemoViewHolder>() {
    // Methods
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val memoView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.memo_item, parent, false)

        return MemoViewHolder(memoView)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        val label = memoList[position].label

        holder.textViewMemoLabel.text = label

        holder.textViewMemoLabel.setOnClickListener {
            val memoContainer =
                (ctx as MainActivity).findViewById<FrameLayout>(R.id.memo_detail_container)

            if (memoContainer == null) {
                val intent = Intent(ctx, DetailActivity::class.java)
                intent.putExtra("memo_detail", label)
                ctx.startActivity(intent)
            } else {
                val fragment = DetailFragment()
                val bundle = Bundle()
                bundle.putString("memo_detail", label)
                fragment.arguments = bundle

                ctx.supportFragmentManager.beginTransaction()
                    .replace(R.id.memo_detail_container, fragment).commit()
            }
        }
    }

    override fun getItemCount(): Int {
        return memoList.size
    }

    fun onItemMove(startPos: Int, endPos: Int): Boolean {
        Collections.swap(memoList, startPos, endPos)
        notifyItemMoved(startPos, endPos)

        return true
    }

    fun onItemDismiss(pos: Int) {
        if (pos > -1) {
            MainRepository().deleteMemo(memoList[pos])
            memoList.removeAt(pos)
            notifyItemRemoved(pos)
        }
    }
}