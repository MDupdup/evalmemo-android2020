package com.formationandroid.evalmemo.di

import android.app.Application
import com.formationandroid.evalmemo.modules.AppModule
import com.formationandroid.evalmemo.repositories.MainRepository
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(mainRepository: MainRepository)
}