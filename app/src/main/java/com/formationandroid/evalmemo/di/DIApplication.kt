package com.formationandroid.evalmemo.di

import android.app.Application

class DIApplication : Application() {

    companion object {
        private lateinit var instance: DIApplication

        fun getAppComponent(): AppComponent {
            return instance.component
        }
    }

    private lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        component = DaggerAppComponent.builder().application(this).build()
    }
}