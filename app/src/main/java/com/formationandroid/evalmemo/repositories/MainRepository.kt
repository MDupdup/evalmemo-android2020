package com.formationandroid.evalmemo.repositories

import com.formationandroid.evalmemo.database.MemoDAO
import com.formationandroid.evalmemo.di.DIApplication
import com.formationandroid.evalmemo.models.Memo
import javax.inject.Inject

class MainRepository {

    @Inject
    lateinit var dao: MemoDAO

    init {
        DIApplication.getAppComponent().inject(this)
    }

    fun fetchMemos(): List<Memo> {
        return dao.fetchMemos()
    }

    fun addMemo(memo: Memo) {
        dao.insertMemo(memo)
    }

    fun deleteMemo(memo: Memo) {
        dao.deleteMemo(memo)
    }
}