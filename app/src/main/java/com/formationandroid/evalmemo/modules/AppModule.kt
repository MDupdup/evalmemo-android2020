package com.formationandroid.evalmemo.modules

import android.app.Application
import android.content.Context
import com.formationandroid.evalmemo.database.MemoDAO
import com.formationandroid.evalmemo.database.helpers.MemoDatabaseHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    companion object {

        @Provides
        fun provideContext(application: Application): Context {
            return application
        }

        @Singleton
        @Provides
        fun provideDAO(c: Context): MemoDAO {
            return MemoDatabaseHelper.getDatabase(c).memoDAO()
        }
    }

}

