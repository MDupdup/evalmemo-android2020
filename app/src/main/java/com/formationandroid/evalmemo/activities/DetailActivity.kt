package com.formationandroid.evalmemo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.formationandroid.evalmemo.DetailFragment
import com.formationandroid.evalmemo.R

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val fragment = DetailFragment()
        val bundle = Bundle()

        bundle.putString("memo_detail", intent.getStringExtra("memo_detail"))
        fragment.arguments = bundle

        supportFragmentManager.beginTransaction().replace(R.id.memo_detail_container, fragment)
            .commit()
    }
}
