package com.formationandroid.evalmemo.activities

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.formationandroid.evalmemo.R
import com.formationandroid.evalmemo.models.Memo
import com.formationandroid.evalmemo.recyclerview.ItemTouchHelperCallback
import com.formationandroid.evalmemo.recyclerview.MemoAdapter
import com.formationandroid.evalmemo.repositories.MainRepository
import com.formationandroid.evalmemo.viewmodels.MainViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var memoList: MutableList<Memo>

    // Layout Bindings
    private lateinit var recyclerView: RecyclerView
    private lateinit var inputBox: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Layout Bindings
        recyclerView = findViewById(R.id.recycler_view_main)
        inputBox = findViewById(R.id.memo_input)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel.init(MainRepository())

        memoList = mainViewModel.fetchMemos().toMutableList()

        // Setting up RecyclerView
        val adapter = MemoAdapter(memoList, this)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val itemTouchHelper = ItemTouchHelper(ItemTouchHelperCallback(adapter))
        itemTouchHelper.attachToRecyclerView(recyclerView)

        recyclerView.adapter = adapter
    }

    fun addMemo(view: View) {
        if (inputBox.text.toString().isNotEmpty()) {
            val newMemo = Memo(inputBox.text.toString())

            mainViewModel.addMemo(newMemo)
            memoList.add(newMemo)

            recyclerView.adapter?.notifyDataSetChanged()

            // Reset input field
            inputBox.setText("")
        }
    }
}
