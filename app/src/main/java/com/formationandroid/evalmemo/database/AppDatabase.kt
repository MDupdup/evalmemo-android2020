package com.formationandroid.evalmemo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.formationandroid.evalmemo.models.Memo

@Database(entities = [Memo::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun memoDAO(): MemoDAO
}