package com.formationandroid.evalmemo.database.helpers

import android.content.Context
import androidx.room.Room
import com.formationandroid.evalmemo.database.AppDatabase

class MemoDatabaseHelper(c: Context) {
    companion object {
        private var dbHelper: MemoDatabaseHelper? = null
        private lateinit var db: AppDatabase

        @Synchronized
        fun getDatabase(c: Context): AppDatabase {
            if (dbHelper == null)
                dbHelper = MemoDatabaseHelper(c.applicationContext)

            return db
        }
    }

    init {
        db = Room
            .databaseBuilder(c, AppDatabase::class.java, "memos.db")
            .allowMainThreadQueries()
            .build()
    }
}