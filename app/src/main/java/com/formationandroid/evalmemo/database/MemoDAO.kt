package com.formationandroid.evalmemo.database

import androidx.room.*
import com.formationandroid.evalmemo.models.Memo

@Dao
abstract class MemoDAO {

    @Query("SELECT * FROM memos")
    abstract fun fetchMemos(): List<Memo>

    @Insert
    abstract fun insertMemo(vararg memos: Memo)

    @Update
    abstract fun updateMemo(vararg memos: Memo)

    @Delete
    abstract fun deleteMemo(vararg memos: Memo)
}