package com.formationandroid.evalmemo.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "memos")
class Memo() {

    @PrimaryKey(autoGenerate = true)
    var memoId: Long = 0
    lateinit var label: String

    constructor(label: String) : this() {
        this.label = label
    }
}