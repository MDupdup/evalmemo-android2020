package com.formationandroid.evalmemo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.formationandroid.evalmemo.models.Memo
import com.formationandroid.evalmemo.repositories.MainRepository

class MainViewModel : ViewModel() {
    private lateinit var mainRepository: MainRepository
    private var liveDataItem: MutableLiveData<Memo> = MutableLiveData()

    fun init(mainRepository: MainRepository) {
        this.mainRepository = mainRepository
    }

    fun addMemo(memo: Memo) {
        mainRepository.addMemo(memo)
    }

    fun fetchMemos(): List<Memo> {
        return mainRepository.fetchMemos()
    }

    fun removeMemo(memo: Memo) {
        mainRepository.deleteMemo(memo)
    }
}